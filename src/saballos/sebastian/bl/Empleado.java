package saballos.sebastian.bl;



public class Empleado {

//	1- atributos 
//	2- constructores 
//	3- Métodos de acceso get y set 
//	4- toString()
	
	
	private String cedula;  
	private String nombre;
	private String puesto;
	
	
	// Constructor 
	
	public Empleado() {

		this.cedula = "";
		this.nombre = "";
		this.puesto = "";
	}

	// Constructor que recibe los parámetros 
	public Empleado(String cedula, String nombre, String puesto) {
		
		this.cedula = cedula;
		this.nombre = nombre;
		this.puesto = puesto;
	}
	
	//METODOS DE ACCESO GET & SET 
	

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	// toString
	public String toString() {
	
		return "cedula=" + cedula + " nombre=" + nombre + " puesto=" + puesto;
		
	}


	
	
}
