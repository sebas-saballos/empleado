package saballos.sebastian.ui;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import saballos.sebastian.bl.Empleado;

public class PrincipalMenuEmpleado {

	//Atributos
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	//Generaci�n Arreglo
	// Al hacer esto es como declarar listaEmpleados como variable globlal
	// En realidad ser�a un atributo de la clase
	static Empleado[] listaEmpleados = new Empleado[10];
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		menu();	
	}

	public static void menu() throws IOException {
		int opcion = 0;
		do {
			System.out.println("*** Bienvenido al sistema ***");
			System.out.println("1. Registar empleado .");
			System.out.println("2. Enlistar empleados");
			System.out.println("3. Salir");
			System.out.println();
			System.out.print("Digite una opci�n: ");
			opcion = Integer.parseInt(in.readLine());
			// llama a metodo procesar opcion
			procesarOpcion(opcion);
		} while (opcion != 3);
	}

	public static void procesarOpcion(int opcionDelMenu) throws IOException {
		switch (opcionDelMenu) {
		case 1:
			registarEmpleado();
			break;
		case 2:
			listarEmpleados();
			break;
		case 3:
			System.out.println();
			System.out.println("Gracias por utilizar nuestro sistema");
			System.exit(0);
		default:
			System.out.println("Opci�n invalida");
		}
	}
	
	public static void registarEmpleado() throws IOException {
		System.out.println("Ingrese la c�dula");
		String cedula = in.readLine();
		System.out.println("Ingrese el nombre");
		String nombre = in.readLine();
		System.out.println("Ingrese el puesto");
		String puesto = in.readLine();
		System.out.println("");
		// Crear objeto y llamar uno de los constructores

		Empleado empleado = new Empleado(cedula, nombre, puesto);

		// Registrarlo en el arreglo
		for (int i = 0; i < listaEmpleados.length; i++) {
			if (listaEmpleados[i] == null) {
				listaEmpleados[i] = empleado;
				// Esto se hace para que no asigne en campos que no se ocupan **PREGUNTAR DE NUEVO**
				// Se puede usar break 
				i = listaEmpleados.length;
			}
		}

	}
	
	static public void listarEmpleados() {
		for (int i = 0; i < listaEmpleados.length; i++) {
			if (listaEmpleados[i] != null) {
				System.out.println("");
				System.out.println(listaEmpleados[i].toString());	
				System.out.println("");
			}
			
			
		}
		
	}
	
}
